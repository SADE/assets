# SADE Assets
This package contains shared resources for SADE, mainly stuff from the template.
SADE is an application for the eXist database that prepares the frontend for
digital editions.
It is used by various projects, e.g. the notebooks of Theodor Fontane.

## Build
`ant`

## Installation
Use your preferd way of getting packages into eXist, e.g.
* put the `*.XAR` file in the folder `$EXIST_HOME/autodeploy`
* install it with the help of the dashboards package manager
* use the newer [package manager application](http://exist-db.org/exist/apps/public-repo/packages/packagemanager.html)

## Credits
This software is provided by SUB Göttingen and created for the DARIAH-DE
project. It is part of the TextGrid infrastructure to support the needs of the
Digital Humanities.
