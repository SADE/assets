We welcome every new feature to the upstream branch!

But at first we ask you to fill in the following information about your feature.
So please take a few minutes to make this great thing even better.

# Summary
This MR provides…

# Use Cases
If you can, please provide use cases for this feature.

# Documentation
Shall we add your feature to the documentation? There is a [feature list](docs/featurelist.md) available.

## Function Documentation
* [ ] Of course I prepared all my functions with an appropriate documentation.

## Are there parts of the documentation we have to adjust?
* [ ] No.
* [ ] Yes, but I'd like someone else to do it.
* [ ] Yes, and I already did!

# Tests
Are we able to test this new feature?
* [ ] Yes, everything can be done via unit tests.
* [ ] No, it is not possible.

# Changelog
* [ ] I added a statement to the CHANGELOG.

# Related Tickets
Add all related issues and especially those to be closed.

## Related
## Closes

# Logs and Screenshots

/cc @mrodzis @mgoebel
