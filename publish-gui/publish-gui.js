var sid;
var sadeUser;
var sadePw;
var page;
var t;
var i;
var test;

$(document).ready(function(){

// general setup (ajax, variables, …)
    $.ajaxSetup({ cache:false });

    $('#sid').change(function() {
        sid=$(this).val();
    });

    $('#user').change(function() {
        sadeUser=$(this).val();
    });

    $('#password').change(function() {
        sadePw=$(this).val();
    });

    forked();

    log("publish engine: ready");

    $('#ajaxLoadIndicator')
    .hide()  // hide it initially
    .ajaxStart(function() {
        $(this).show();
    })
    .ajaxStop(function() {
        $(this).hide();
    });

    // show drag and drop area on linux systems
    if(navigator.platform.toLowerCase().indexOf('linux') >= 0) {
        log('Linux OS detected, showing drag and drop notification');
        $('#droptarget').addClass('drop_enabled');
    } else {
        $('#droptarget').addClass('drop_disabled');
    }

});

function setSid(val) {
    $('#authform').hide();
	sid=val;
	log('got a SessionId '+sid);
}

function setAuth(user, pw) {
    $('#authform').hide();
    sadeUser = user;
    sadePw = pw;
    $("input[name=forkUser]").val(sadeUser);
    $("input[name=forkPassword]").val(sadePw);
    log('got credentials to the database for user '+user);
}

function publish() {
	log('starting...');

	$('#logbox').show();
	$("table#main").find("tr[id^='uri']").each(function() {
		var uri = $(this).children()[2].textContent;
		var uriId = $(this).attr("id");
        if(sadeUser === undefined) {
            sadeUser = $('#user').val();
            sadePw = $('#password').val();
            sid = $('#sid').val();
        }

		var sidreq = sid ? "&sid="+sid : "";
        var target="data";

		$.ajax({
		    type: 'POST',
			url: 'modules/textgrid/publish.xql',
			data: {'uri': uri,  'target': target, 'user': sadeUser, 'password': sadePw, 'sid': sid},
			cache: false,
			success: function(xml) {
                test = $(xml).find('message');
			    if($(xml).find('ok').text()) {
			        $(xml).find('ok').each( function(){
			                log('success: ' + $(this).text() );
                        });
            $('#'+uriId).removeClass("danger");
    				$('#'+uriId).addClass("success");
    				// lets check for a validation report:
    				vali = $(xml).find('status').text();
    				if (vali.length > 0){
			        log('validation status: ' + vali);}
              for (i = 0; i < test.length; i++) {
              $('#log').append( test[i] );
              $('#log').append( "<br/>" );
              }
			    } else {
			        errnote = $(xml).find('error').text();
			        log('error: ' + errnote);
      				$('#'+uriId).addClass("danger");
              $('#'+uriId).removeClass("success");
			    }
			},
			error: function(xml){
			    message = xml.responseXML.childNodes["0"].childNodes[1].textContent
			    log('<span style="color:red;">ERROR: ' + message + '</span>');
  				$('#'+uriId).addClass("danger");
          $('#'+uriId).removeClass("success");
			}
		});
	});
}

function addTGObject(uri, title, contentType) {
    if( uri.split(".").length === 3 ) {
        // this is a workaround a bug in the Navigator. it drops an incorrect URI
        uri = uri.split(".")[0] + "." + uri.split(".")[1];
    }
	var shortUri = uri;
	if(uri.beginsWith('textgrid:')) {
		var shortUri = uri.substring(9);
	}

	var uriId = 'uri'+shortUri.replace("\.", "_");

	/* if element id already there, do not add again, blink instead */
	if($('#'+uriId).length > 0) {
		$('#'+uriId).addClass('blink');
		setTimeout(function() { $('#'+uriId).removeClass('blink'); }, 1000);
		return;
	}
	else {
    	$('table#main tbody').append(
            '<tr id="'+ uriId +'"><td>'+ title.substr(0, title.indexOf(' (')) + '</td><td>'+ contentType +'</td><td>'+ uri +'</td><td><i class="fas fa-trash" onclick="$(this).parent().parent().remove();"></i></td></tr>');
        $('table#main').css("color", "black");
	}
}

function getMimeClass(contentType) {
	if(contentType.beginsWith('text/tg.collection+tg.aggregation')) {
		return 'mime_collection';
	} else if (contentType.beginsWith('text/tg.edition+tg.aggregation')) {
		return 'mime_edition';
	} else if(contentType.indexOf('tg.aggregation') != -1) {
		return 'mime_aggregation';
	} else if (contentType.beginsWith('text/tg.work+xml')) {
		return 'mime_work';
	} else if (contentType.beginsWith('text/xml')) {
		return 'mime_xml';
	} else if (contentType.beginsWith('text/linkeditorlinkedfile')) {
		return 'mime_tble';
	} else if (contentType.beginsWith('image')) {
		return 'mime_image';
	} else {
		return 'mime_unknown';
	}
}

function reset() {
	$('table#main tbody').empty();
	$('table#main tbody').append('<tr><td colspan="4" style="text-align: center;font-size:150%;color:grey;"><div id="droptarget" ondrop="drop(event)" ondragover="allowDrop(event)"></div></td></tr>');
	$('table#main').css("color", "grey");
	$('#viewData').empty();
}

String.prototype.beginsWith = function (string) {
    return(this.indexOf(string) === 0);
}

function format(num){
    if (num < 10) { return "0" + num; }
    else { return num }
}

function log(string) {
    var date = new Date();
    var hour = format(date.getHours());
    var min = format(date.getMinutes());
    var sec = format(date.getSeconds());
    var timeStamp = hour + ":" + min + ":" + sec;
    $('#log ul').append('<li><span>'+ timeStamp +'</span>' + string + '</li>');
    $( "#log" ).scrollTop(1000000);
}
function view(string) {
    $('#view').prepend(string + '<hr/>');
}
function links(string) {
    $('#view').prepend(string + '<hr/>');
}

function forked() {
  var newpw = getUrlParameter('newpw');
  if( newpw ) {
    $("#newpw").text(newpw);
    $("url").text( window.location.origin + window.location.pathname );
    $("#fork-successull").show();
  }
}

function allowDrop(ev) {
  ev.preventDefault();
  ev.stopPropagation();
}

function drop(ev) {
  var data = ev.dataTransfer.getData("text/html");
  var uri = $(data).attr('uri');
  var title = $(data).text();
  var type = $(data).attr('type');
  log(uri + "|" + title + '|' +type);
  addTGObject(uri, title, type);
}

// https://stackoverflow.com/a/21903119
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};
