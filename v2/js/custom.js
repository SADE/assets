/**
 * Custom JS
 */

/*** Navbar active links ***/

$(function() {

  function getPageName(url) {
    url = url.split("/");
    url = url[url.length - 1];
    url = url.split("#");
    url = url[0];
    return url;
  }

  var currentUrl = window.location.href;
  var pageName = getPageName(currentUrl);

  $(".navbar-nav li > a[href$='" + pageName + "']").parent("li").first()
    .addClass("active");

  $(".navbar-nav > li").has(".active").first()
    .addClass("active");

});

/** Search Form Focus **/
$("#searchInput").parent().click(function(){
  $("#searchInput").focus();
});

/*** Sticky footer ***/

$(function() {
  function stickyFooter() {
    var footer = $("footer");
    var footerHeight = footer.outerHeight(true);
    $("body").css("margin-bottom", footerHeight);
  };

  setTimeout(stickyFooter,200);

  $(window).resize(function() {
    setTimeout(stickyFooter, 200);
  });
});


/*** Showcase carousel animations ***/

$(function() {

  function addAnimation(parent) {
    var elements = $(parent).find("[data-animation]");

    elements.each(function() {
      var animation = $(this).data("animation");
      $(this).addClass(animation);
    });
  };

  function removeAnimation(parent) {
    var elements = $(parent).find("[data-animation]");

    elements.each(function() {
      var animation = $(this).data("animation");
      $(this).removeClass(animation);
    });
  };

  $('#showcase-carousel').on({
    "slid.bs.carousel": function () {
      addAnimation("#showcase-carousel .item.active");
    },
    "slide.bs.carousel": function () {
      setTimeout(function() {
        removeAnimation("#showcase-carousel .item.active");
      }, 600);
    }
  });

});


/*** Smooth scroll to anchor ***/

$(function() {
  $("a[href*='#']").not("[href='#'], [data-toggle], [data-slide]").click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

      if (target.length) {
        $('html,body').animate({
          scrollTop: (target.offset().top - 20)
        }, 1000);

        return false;
      }
    }
  });
});
