xquery version "3.1";

declare namespace xhtml="http://www.w3.org/1999/xhtml";
declare namespace xsl="http://www.w3.org/1999/XSL/Transform";

(: The function processing the TEI version works if we use the stylesheets as a
 : standalone, but the path the function uses for finding the VERSION file is
 : interpreted als db internal path by eXist-db -- which leads to errors. Thus
 : we remove the TEI function here.:)
let $functions := doc("/db/apps/sade_assets/TEI-Stylesheets/common/functions.xsl")
let $empty-otherwise := <xsl:otherwise/>
let $replace := update replace $functions//xsl:function[@name = "tei:stylesheetVersion"]//xsl:otherwise with $empty-otherwise

let $textstructure := doc("/db/apps/sade_assets/TEI-Stylesheets/html/html_textstructure.xsl")
let $replacement-text := <xsl:text>stylesheet version not available</xsl:text>
let $replace := update replace $textstructure//xsl:template[@name = "stdfooter"]//xsl:value-of[@select = "tei:stylesheetVersion(/)"] with $replacement-text

let $log :=
    if($functions//xsl:function[@name = "tei:stylesheetVersion"]//xsl:otherwise/*) then
        util:log-system-out("Omission of stylesheet version NOT successfull.")

    else
        util:log-system-out("Successfully omitted stylesheet version.")

(: The stdheader functionality is not used in SADE's template. To avoid clutter
 : in the resulting HTML we decided to cut out the stdheader completely (instead
 : of just making it invisible in a *.css). :)
let $files-with-stdheader := collection("/db/apps/sade_assets/TEI-Stylesheets/html")[.//*[@name ="stdheader"]]
let $omit-stdhdr :=
    for $file in $files-with-stdheader return
        (update delete $file//xhtml:div[matches(@class, "stdheader")],
        update delete $file//*[@name ="stdheader"])

return
    if(collection("/db/apps/sade_assets/TEI-Stylesheets/html")[.//*[@name ="stdheader"]]) then
        util:log-system-out("Omission of stdheader NOT successfull.")

    else
        util:log-system-out("Successfully omitted stdheader.")
